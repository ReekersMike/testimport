#!/bin/bash
sudo apt update
sudo apt install python3 python3-pip -y
sudo pip install poetry flask
git clone https://gitlab.com/ReekersMike/devopschess/
cd devopschess/backend/
sudo poetry install 
(sudo poetry run python3 app.py&)
