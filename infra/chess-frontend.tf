#bucket for frontend
resource "aws_s3_bucket" "chess_frontend_bucket" {
  bucket = "mike449904chessfrontendbucket"
  force_destroy = true
  tags = {
    Name        = "mikes449904chessfrontendbucket"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_website_configuration" "config" {
  bucket = aws_s3_bucket.chess_frontend_bucket.id

  index_document {
    suffix = "index.html"
  }

  routing_rule {
    condition {
      key_prefix_equals = "games"
    }
    redirect {
      host_name = "${aws_lb.backend_lb.dns_name}:5000"
      protocol = "http"
      replace_key_with = "games"
    }
  }
}

resource "aws_s3_bucket_policy" "allow_access" {
  bucket = aws_s3_bucket.chess_frontend_bucket.id
  policy = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::mike449904chessfrontendbucket/*"
        }
    ]
  }
EOF
}

resource "aws_s3_object" "index" {
  bucket = aws_s3_bucket.chess_frontend_bucket.id
  key    = "index.html"
  source = "../frontend/dist/index.html"
  content_type = "text/html"
}

resource "aws_s3_object" "css" {
  bucket = aws_s3_bucket.chess_frontend_bucket.id
  key    = "assets/index.495d1106.css"
  source = "../frontend/dist/assets/index.495d1106.css"
  content_type = "text/css"
}

resource "aws_s3_object" "js" {
  bucket = aws_s3_bucket.chess_frontend_bucket.id
  key    = "assets/index.aea488c9.js"
  source = "../frontend/dist/assets/index.aea488c9.js"
  content_type = "application/x-javascript"
}