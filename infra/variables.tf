variable "subnet_details" {
  description = "Subnet Detials"
  type        = map
  default = {
    "us-east-1c" = "10.0.3.0/24"
    "us-east-1d" =  "10.0.4.0/24"
  }
}