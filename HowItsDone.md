# How did do things

#### Note
did my best to place all the info I looked at the bottum of the used fiels.

## Exercise 1
- So first I started by googling how to read the file from disk in bash.
- After this I started figuring out how I would like to group the info, so grouping the gameinfo in a GAMEINFO variable and the moves in a MOVES variable.
- After this I starting figuring out how to modify the string to suit the pattern needed to use it in a JSON object.
- Added the curl part of the exercise
- debugging why backend was refusing data -> malformed json -> changed the way I was modifying it and started using 'tr' and 'sed'

## Exercise 2
- Make docker-compose.yml file based on what I made in week 4 of this subject.
- Modified Dockerfile of backend to install needed dependencies
- added entrypoint.sh to backend to use as CMD in Dockerfile
- Created reverse proxy config for nginx frontend
- Made dockerize.sh to run 'docker-compose up -d', 'docker-compose down' and 'docker-compose build'.
- Updated my local node version to be able to build the frontend
- installed the missing dependency as mentioned in the 'clarification' we received in the mail.
- made frontend look at /dist created by build.

## Exercise 3.
https://medium.com/@chrischuck35/how-to-create-a-mysql-instance-with-docker-compose-1598f3cc1bee
- Used mentioned 'tutorial' as a template for the docker-compose part for mysql.
- used volume to persist data between container creation and destruction.
- modified app.py to fix the string length issues.

## Exercise 4.
- Used the gitlab-ci.yml I made in week 3 as a template for the one I made here.
- Got the ci to run the test. 
- after this I kept running it and adding modules that it needed to actually run the tests.
- at the end I told it to save the output of the test as an artifact.

## Exercise 5 & 7
https://stackoverflow.com/questions/58001764/how-to-split-a-terraform-file-main-tf-in-several-files-no-modules
https://upcloud.com/resources/tutorials/terraform-variables
https://vitejs.dev/guide/env-and-mode.html
https://medium.com/idomongodb/how-to-npm-run-start-at-the-background-%EF%B8%8F-64ddda7c1f1
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket
https://stackoverflow.com/questions/54413723/terraform-aws-how-to-get-dns-name-of-load-balancer-by-its-arn
- Decided to split the terraform main.tf into 4 seperate .tf files, to make it more readable. (using stuff from week 6.)
- found out that I can not append a Scaling group to 2 subnets in the same availability zone, so I decided to use the wanted availability zone as the key in a variables map object that I use to create the 2 subnets for the VPC.
- modified the security group for the instances that are launched by the scaling group to only allow access on CIDR_BLOCK 10.0.3.0/24 and 10.0.4.0/24 so these can only be accessed from inside the 2 subnets.
- added a bucket to the terraform config.
- added 3 aws_s3_object recourses to upload the files to the bucket
- added a aws_s3_bucket_policy as descriped in the week 5 exercises.
- added force_destroy flag to the bucket so it can be destroyed even if there are files in it.
- changed url in the frontend api.js so it calls the url for the s3 bucket.
- A improvement could be made to the backend, as currently it is using its internal database, so they can have different data
- actual display of the chess game does not seem to work with the dist when served as a static web page, maybe im missing something with dependencies but they seem to all be in the package.json
- the exam document talks about that you should see different hostnames on the app, but I dont understand how it would show different ones if I am calling the load balancer DNS name.

## Exercise 6.
https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html
https://stackoverflow.com/questions/34228864/stop-and-delete-docker-container-if-its-running
- Look into how to build a docker image with gitlab ci.
- started fooling around with it 
- found mentioned link, applied that and tested if it worked
- made a proper build job for the image
- started on a deploy job where we ssh into the aws container
- got ssh working with ci var as file
- used second link 'trick' to always try to shutdown container before creating it, so that the CI keeps working after the first time
- It would have been nicer if I incorporated awscli into the gitlab ci so that I could query the url of the ci instance used to deploy the backend. but for now its just hardcoded and needs to be changed.
