const url = 'http://mike449904chessfrontendbucket.s3-website-us-east-1.amazonaws.com/' // backend running here

const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

export default {
    // Get all games in db
    getGames: () => {
        const options = {
            method: 'GET',
            headers: headers
        }
        return fetch(url + 'games', options).then(response => {
            if (response.ok) {
                return response.json();
            }
            throw response; // error
        });
    }
}
