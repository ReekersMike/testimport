URL=$1

if [ $# = 0 ]
then
    echo "Please supply a url for posting the game data."
else
	GAMEINFO=""
	MOVES=""
	CHESSGAMESLOC="./chess.pgn"
	while read LINE; do #read the lines of the file
		# if the line contains [ then we do this if statement
		if [[ $LINE == *[* ]]; then\
			#The first tr replaces all [ with ", then the sed looks for the first instance of a ' ' in the string and then replaces the first instance of a ' ' in the string with a ":
			# then the second tr replaces all ] with ,
			GAMEINFO+=`echo $LINE | tr '[' '"' | sed '0,/ /{s/ /":/}' | tr ']' ','`
		elif [[ $LINE == *1* ]]; then
			MOVES=${LINE}
			#the %, is used to truncate the last , at the end of the GAMEINFO variable.
			GAMEINFO=${GAMEINFO%,}

			#construct JSON
			JSON="{ \"type\":\"Game\", \"fields\":{ $GAMEINFO }, \"moves\": \"$MOVES\"}"

			#DO CURL
			echo $URL
			curl -X POST $URL -H "Content-Type: application/json" -d "$JSON"	
			
			MOVES=""
			GAMEINFO=""
		fi		
	done < $CHESSGAMESLOC
fi

#https://linuxize.com/post/how-to-check-if-string-contains-substring-in-bash/
#https://www.baeldung.com/linux/add-newline-variable-bash
#https://linuxize.com/post/bash-concatenate-strings/
#https://stackoverflow.com/questions/1521462/looping-through-the-content-of-a-file-in-bash
#https://stackoverflow.com/questions/27658675/how-to-remove-last-n-characters-from-a-string-in-bash
#https://reqbin.com/req/curl/c-dwjszac0/curl-post-json-example
#https://unix.stackexchange.com/questions/654567/reassigning-a-variable-using-tr-and-getting-rid-of-new-line-and-carriage-returns
#https://stackoverflow.com/questions/2871181/replacing-some-characters-in-a-string-with-another-character
#https://stackoverflow.com/questions/148451/how-to-use-sed-to-replace-only-the-first-occurrence-in-a-file
#https://askubuntu.com/questions/1162945/how-to-send-json-as-variable-with-bash-curl
