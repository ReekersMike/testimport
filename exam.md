
# Toetsopdracht DevOps
# Maart 2023

In deze eindopdracht ontwerp en implementeer je een infrastructuur, en
ontwikkelomgeving, voor een webapplicatie waarmee gebruikers
schaakpartijen van wereldberoemde schakers kunnen bekijken.
De webapplicatie maakt verbinding met een backend die geschreven is in
Flask. De gegevens van de partijen worden opgeslagen in deze backendapplicatie.

De backend gebruikt daarvoor een SQLite-database.
Jouw taak is als eerste om deze applicatie uit te voeren en te testen.
Daarnaast moet je een continous integration pipeline voor deze applicatie
opzetten en de applicatie containeriseren.

## Voorwaardelijke eisen
• Het is verplicht om alle oefenopdrachten af te laten tekenen door je docent, voordat je de
eindopdracht kan inleveren.
• Voor elke requirement (zie hieronder) moet je uitleggen hoe je het probleem hebt opgelost
en waarom je het op deze manier hebt opgelost. Dit moet in je eigen woorden worden
beschreven op een manier die begrijpelijk is voor de docent die je werk beoordeelt. Je kunt dit
opschrijven in een Markdown-bestand in je repository.
• Upload een gezipte export van je Gitlab-project naar Blackboard (Instellingen → Algemeen
→ Geavanceerd → Exportproject). Vermeld ook de URL van je repository in je inlevering.
Toetsregels
• De deadline voor het inleveren van de opdracht is maandag 17 april 9:00 (week 3.9).
• De opdracht wordt gedaan in groepjes van 1 of 2 studenten die zijn ingeschreven op
Blackboard.
• Je cijfer wordt bepaald op basis van het mondelinge examen dat plaatsvindt nadat je je
opdracht hebt ingeleverd.
• Je krijgt alleen punten voor elementen uit je werk die je tijdens het mondeling examen kunt
uitleggen. Bijvoorbeeld: als je code werkt, maar je kunt niet uitleggen wat het doet of
waarom het zo werkt, zal het niet meetellen voor je eindcijfer.
• Het is niet toegestaan om hulp te krijgen van iemand (mens of AI) buiten je eigen groepje,
behalve van je docent. Het is ook niet toegestaan om hulp te verlenen aan iemand anders.
• Je mag online berichten, artikelen, tutorials, boeken, video's gebruiken. Je moet wel
bronvermelding toevoegen voor alle bronnen en codefragmenten die je in je tekst/code
gebruikt.

## Functionele requirements
### 1. Voeg gegevens toe aan de backend

In de template zip vind je een map met een backendapplicatie. Dit is een door ons geschreven Flaskapplicatie
die gegevens in JSON-formaat teruggeeft. Als je de applicatie uitvoert (met Poetry) zou je
het volgende bericht moeten zien wanneer je de browser op localhost opent:
{"host":"localhost","message":"Successfully connected to the backend." }

Als je naar http://localhost:5000/games krijg je een lijst met games (die op dit moment leeg zou
moeten zijn):
{"host":"127.0.0.1:5000","message":"Successfully connected to the backend." ,"games":[]}
Om games aan de backend toe te voegen, moet je een POST-request doen met het juiste format. Je
kunt curl gebruiken om zo'n POST-request te doen. Zorg ervoor dat je het content type instelt op
application/json in je Curl commando.
In de template zip vind je een Bash-script met de naam add_games.sh. Bewerk dit script zodat
het een lijst met games naar de backend stuurt. De lijst moet worden gelezen uit het bestand
chess.pgn.

### 2. Containeriseer backend en frontend applicaties

De backend- en frontend-applicatie moeten worden gecontaineriseerd met Docker en Docker
Compose. Er moeten aparte backend- en frontend-containers gemaakt worden. De frontend moet
worden geserveerd met Nginx.
Om op een makkelijke manier de containers te kunnen bouwen, uit te voeren en te stoppen schrijf je
een script. Zorg ervoor dat tijdelijke bestanden die zijn gemaakt door de applicatie buiten Docker uit
te voeren, niet in de image terechtkomen.

### 3. Containerisatie van een externe database
Breid je compose-bestand uit zodat de gegevens niet in SQLite in de backend worden opgeslagen,
maar in een externe MySQL-database (die als een container wordt uitgevoerd). Om dit te laten
werken, moet je de volgende environment variabelen instellen in de backend en de MySQLcontainer:
MYSQL_USER
MYSQL_PASSWORD
MYSQL_DATABASE
MYSQL_ROOT_PASSWORD (deze variabele is alleen nodig in de MySQL-container)
In de backend-container moet je ook de volgende environment variabelen instellen:
• FLASK_ENV (stel de waarde in op 'production' zodat Flask weet dat het in productiemodus
moet draaien)
• MYSQL_HOST (stel de naam in van de server waarmee de backend verbinding moet maken)

### 4. Continuous Integration

Zet continuous integration op voor de app, zodat de meegeleverde unit tests automatisch worden
uitgevoerd wanneer wijzigingen naar de repository worden gepusht. Daarnaast moet je de
resultaten voor de unit tests opslaan als een artefact in Gitlab (met een vervaldatum van een
maand).

### 5. Maak een infrastructuur om de applicatie uit te voeren
Creëer een geschikte infrastructuur voor de applicatie in AWS met behulp van Terraform. De
infrastructuur hoeft (nog) niet high availability te zijn. Lukt dat niet met Terraform, dan mag je de
infrastructuur handmatig aanmaken (dan verlies je natuurlijk wel punten). Je Terraform-configuratie
moet in de meegeleverde infra map worden geplaatst .
De backend moet worden geïnstalleerd op een EC2-instance (voorlopig handmatig). De frontend kan
worden geserveerd als een statische website.

### 6. Automatiseer de deployment van de applicatie
Automatiseer de deployment van de backend, zodat wanneer we committen naar de main branch,
Gitlab CI:
• de benodigde Docker image(s) maakt
• de image uploadt het naar het private container registry van jouw Gitlab-repository
• verbinding maakt met de EC2-instance via SSH, de Docker image pullt en de backend start.

Hiervoor zul je de inhoud van je AWS private SSH key (die je gebruikt om in te loggen op de instance)
moeten opslaan in een environment variabele in Gitlab CI (Settings -> CI/CD -> Variables, klik op
"Expand"). In je .gitlab-ci.yml bestand kun je deze variabele gebruiken om verbinding te
maken met de EC2-instance met behulp van SSH.
Bij inloggen met SSH controleert de client of de server een bekende server is. Voor onze
geautomatiseerde implementatie is dit niet erg handig (omdat iemand dan steeds 'ja' moet intypen).
Om deze controle uit te schakelen, kun je de optie -o StrictHostKeyChecking=no
meegeven aan SSH.
### 7. Maak een backend met high availability
Breid je Terraform configuratie uit met een high availability backend. Zorg ervoor dat de instances
niet rechtstreeks toegankelijk zijn van buiten jouw netwerk.
Vergeet niet om de frontend aan te passen, zodat deze verbinding maakt met je aangepaste
infrastructuur. Om te controleren of je infrastructuur correct is opgezet, check je de 'Host'-
informatie die in de frontend wordt weergegeven. Je zou verschillende hostnamen moeten zien bij
het opnieuw laden van de pagina.

## Proces requirements
Tijdens het uitvoeren van de opdracht moet je een goed softwareontwikkelingsproces volgen. Dit
houdt onder andere in dat je taken bijhoudt met Gitlab Issues, dat je Git gebruikt voor het
samenwerken aan de code, en dat je branches en merge requests gebruikt om code reviews te
kunnen doen. Dit zou allemaal duidelijk moeten blijken uit je Gitlab repository.